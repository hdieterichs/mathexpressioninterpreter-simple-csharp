namespace Hediet.SimpleMathExpressionInterpreter.AbstractSyntaxTree
{
    public abstract class AstNode
    {
        protected AstNode(int column)
        {
            Column = column;
        }

        public abstract T Visit<T>(IAstVisitor<T> visitor);

        public int Column { get; private set; }
    }
}