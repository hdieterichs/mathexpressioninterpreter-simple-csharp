namespace Hediet.SimpleMathExpressionInterpreter.AbstractSyntaxTree
{
    public class BinaryExpression : InvocationExpression
    {
        public BinaryExpression(string method, AstNode left, AstNode right, int column)
            : base(method, new AstNode[] { left, right }, column)
        {
            Left = left;
            Right = right;
        }

        public AstNode Left { get; private set; }
        public AstNode Right { get; private set; }

        public override string ToString()
        {
            return string.Format("{1} {0} {2}", Method, Left, Right);
        }
    }
}