using System;

namespace Hediet.SimpleMathExpressionInterpreter.AbstractSyntaxTree
{
    public class ConstantExpression: AstNode
    {
        public string Identifier { get; private set; }

        public ConstantExpression(string identifier, int column)
            : base(column)
        {
            if (string.IsNullOrEmpty(identifier)) 
                throw new ArgumentException("identifier");
            Identifier = identifier;
        }

        public override T Visit<T>(IAstVisitor<T> visitor)
        {
            return visitor.VisitConstantExpression(this);
        }

        public override string ToString()
        {
            return Identifier;
        }
    }
}