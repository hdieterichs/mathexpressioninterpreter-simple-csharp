namespace Hediet.SimpleMathExpressionInterpreter.AbstractSyntaxTree
{
    public interface IAstVisitor<T>
    {
        T VisitInvocationExpression(InvocationExpression invocationExpression);
        T VisitNumberExpression(NumberExpression numberExpression);
        T VisitConstantExpression(ConstantExpression constantExpression);
        T VisitParenthesisExpression(ParenthesisExpression parenthesisExpression);
    }
}