using System;
using System.Linq;

namespace Hediet.SimpleMathExpressionInterpreter.AbstractSyntaxTree
{
    public class InvocationExpression : AstNode
    {
        public string Method { get; private set; }
        public AstNode[] Args { get; private set; }

        public InvocationExpression(string method, AstNode[] args, int column)
            : base(column)
        {
            if (string.IsNullOrEmpty(method)) 
                throw new ArgumentException("method");
            if (args == null) 
                throw new ArgumentNullException("args");

            Method = method;

            if (args.Any(astNode => astNode == null))
                throw new ArgumentException("args");
  
            Args = args;
        }

        public override T Visit<T>(IAstVisitor<T> visitor)
        {
            return visitor.VisitInvocationExpression(this);
        }

        public override string ToString()
        {
            return string.Format("{0} ({1})", Method, string.Join<AstNode>(", ", Args));
        }
    }
}