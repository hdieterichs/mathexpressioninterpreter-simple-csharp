using System.Globalization;

namespace Hediet.SimpleMathExpressionInterpreter.AbstractSyntaxTree
{
    public class NumberExpression : AstNode
    {
        public double Value { get; private set; }

        public NumberExpression(double value, int column)
            : base(column)
        {
            Value = value;
        }

        public override string ToString()
        {
            return Value.ToString(CultureInfo.InvariantCulture);
        }

        public override T Visit<T>(IAstVisitor<T> visitor)
        {
            return visitor.VisitNumberExpression(this);
        }
    }
}