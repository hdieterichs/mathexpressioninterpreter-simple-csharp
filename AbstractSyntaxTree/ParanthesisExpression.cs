using System;

namespace Hediet.SimpleMathExpressionInterpreter.AbstractSyntaxTree
{
    public class ParenthesisExpression : AstNode
    {
        public AstNode Expression { get; private set; }

        public ParenthesisExpression(AstNode expression, int column)
            : base(column)
        {
            if (expression == null)
                throw new ArgumentNullException("expression");
            Expression = expression;
        }

        public override T Visit<T>(IAstVisitor<T> visitor)
        {
            return visitor.VisitParenthesisExpression(this);
        }

        public override string ToString()
        {
            return string.Format("({0})", Expression);
        }
    }
}