using System;

namespace Hediet.SimpleMathExpressionInterpreter.AbstractSyntaxTree
{
    public class UnaryExpression : InvocationExpression
    {
        public UnaryExpression(string method, AstNode argument, int column)
            : base(method, new AstNode[] { argument }, column)
        {
            if (argument == null)
                throw new ArgumentNullException("argument");
            Argument = argument;
        }

        public AstNode Argument { get; private set; }

        public override string ToString()
        {
            return string.Format("{0}{1}", Method, Argument);
        }
    }
}