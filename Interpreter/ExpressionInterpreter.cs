﻿using System;
using System.Linq;
using Hediet.SimpleMathExpressionInterpreter.AbstractSyntaxTree;
using Hediet.SimpleMathExpressionInterpreter.Logger;

namespace Hediet.SimpleMathExpressionInterpreter.Interpreter
{
    public class ExpressionInterpreter : IAstVisitor<double>
    {
        private readonly ILogger logger;

        public ExpressionInterpreter(ILogger logger)
        {
            if (logger == null)
                throw new ArgumentNullException("logger");

            this.logger = logger;
        }

        public double VisitInvocationExpression(InvocationExpression invocationExpression)
        {
            double[] args = invocationExpression.Args.Select(a => a.Visit(this)).ToArray();
            string method = invocationExpression.Method;

            if (method == "-" && args.Length == 1)
            {
                var result = -args[0];
                logger.Log(string.Format("{0} {1} = {2}", method, args[0], result));
                return result;
            }

            if ("+-*/".Contains(method))
            {
                if (args.Length != 2)
                    throw new InterpretException(string.Format("Invalid argument count for '{0}'", method), invocationExpression.Column);

                double result = 0;

                if (method == "*")
                    result = args[0] * args[1];
                else if (method == "/")
                {
                    if (args[1] == 0)
                        throw new InterpretException("Cannot divide by zero", invocationExpression.Column);
                    result = args[0]/args[1];
                }
                else if (method == "+")
                    result = args[0] + args[1];
                else if (method == "-")
                    result = args[0] - args[1];

                logger.Log(string.Format("{0} {1} {2} = {3}",  args[0], method, args[1], result));

                return result;
            }

            if (method == "sin" || method == "tan")
            {
                if (args.Length != 1)
                    throw new InterpretException(string.Format("Invalid argument count for '{0}'", method), invocationExpression.Column);

                double result = 0;

                if (method == "sin")
                    result = Math.Sin(args[0]);
                else if (method == "tan")
                    result = Math.Tan(args[0]);

                logger.Log(string.Format("{0}({1}) = {2}", method, args[0], result));

                return result;
            }

            throw new InterpretException(string.Format("Operator '{0}' not found", method), invocationExpression.Column);
        }

        public double VisitNumberExpression(NumberExpression numberExpression)
        {
            return numberExpression.Value;
        }

        public double VisitConstantExpression(ConstantExpression constantExpression)
        {
            switch (constantExpression.Identifier)
            {
                case "PI": 
                    return Math.PI;
                case "e":
                    return Math.E;
                default:
                    throw new InterpretException(string.Format("Constant '{0}' not found", 
                        constantExpression.Identifier), constantExpression.Column);
            }
        }

        public double VisitParenthesisExpression(ParenthesisExpression parenthesisExpression)
        {
            return parenthesisExpression.Expression.Visit(this);
        }
    }
}