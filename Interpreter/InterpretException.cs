﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hediet.SimpleMathExpressionInterpreter.Interpreter
{
    class InterpretException : TextException
    {
        public InterpretException(string message, int column)
            : base(message, column)
        {

        }
    }
}
