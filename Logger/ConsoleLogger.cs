﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hediet.SimpleMathExpressionInterpreter.Logger
{
    public class ConsoleLogger : ILogger
    {
        private int step = 0;

        public void Log(string message)
        {
            step++;
            Console.WriteLine("{0}. Step: {1}", step, message);
        }
    }
}
