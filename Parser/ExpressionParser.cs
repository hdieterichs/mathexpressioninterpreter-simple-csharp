using System;
using System.Globalization;
using Hediet.SimpleMathExpressionInterpreter.AbstractSyntaxTree;
using Hediet.SimpleMathExpressionInterpreter.Tokenizer;

namespace Hediet.SimpleMathExpressionInterpreter.Parser
{
    public class ExpressionParser
    {
        private ParenthesisExpression ParseParenthesisExpression(TokenReader tokenReader) // (
        {
            AstNode node = ParseExpression(tokenReader, null);
            tokenReader.MoveNext();

            if (tokenReader.Current.Type != TokenType.RightParenthesis)
                throw new ParseException(string.Format("Right parenthesis expected, but got {0}", tokenReader.Current), tokenReader.Current.Column);

            return new ParenthesisExpression(node, tokenReader.Current.Column);
        }

        private InvocationExpression ParseInvocationExpression(TokenReader tokenReader, string method) // identifier
        {
            tokenReader.MoveNext();

            if (tokenReader.Current.Type != TokenType.LeftParenthesis)
                throw new ParseException(string.Format("Left parenthesis expected, but got {0}", tokenReader.Current), tokenReader.Current.Column);

            AstNode node = ParseExpression(tokenReader, null);
            tokenReader.MoveNext();

            if (tokenReader.Current.Type != TokenType.RightParenthesis)
                throw new ParseException(string.Format("Right parenthesis expected, but got {0}", tokenReader.Current), tokenReader.Current.Column);

            return new InvocationExpression(method, new AstNode[] { node }, tokenReader.Current.Column);
        }


        private AstNode ParseTerm(TokenReader tokenReader)
        {
            if (!tokenReader.MoveNext())
                throw new ParseException("Identifier, left paranthesis or number expected, but got end of line", tokenReader.Length);

            Token token = tokenReader.Current;

            AstNode expression;

            if (token.Type == TokenType.Identifier)
            {
                string identifier = token.Symbol;
                Token nextToken = tokenReader.Peek();

                if (nextToken != null && nextToken.Type == TokenType.LeftParenthesis)
                    expression = ParseInvocationExpression(tokenReader, identifier);
                else
                    expression = new ConstantExpression(identifier, token.Column);
            }
            else if (token.Symbol == "-")
                expression = new UnaryExpression(token.Symbol, ParseTerm(tokenReader), token.Column);
            else if (token.Type == TokenType.LeftParenthesis)
                expression = ParseParenthesisExpression(tokenReader);
            else if (token.Type == TokenType.Number)
                expression = new NumberExpression(double.Parse(token.Symbol, CultureInfo.InvariantCulture), token.Column);
            else
                throw new ParseException(string.Format("Identifier, left paranthesis or number expected, but got {0}", token), token.Column);

            return expression;
        }

        class OperatorComparable : IComparable<string>
        {
            private readonly int thisOperatorPriority;

            private int GetOperatorPriority(string op)
            {
                if (op == "*" || op == "/")
                    return 100;
                if (op == "+" || op == "-")
                    return 50;
                else
                    return 0;
            }

            public OperatorComparable(string thisOperator)
            {
                if (thisOperator == null) throw new ArgumentNullException("thisOperator");
                thisOperatorPriority = GetOperatorPriority(thisOperator);
            }

            public int CompareTo(string other)
            {
                return thisOperatorPriority.CompareTo(GetOperatorPriority(other));
            }
        }

        private AstNode ParseExpression(TokenReader tokenReader, IComparable<string> operatorPriority)
        {
            AstNode expression = ParseTerm(tokenReader);

            while (true)
            {
                Token nextToken = tokenReader.Peek();

                if (nextToken == null || nextToken.Type == TokenType.RightParenthesis)
                    return expression;
                else
                {
                    if (nextToken.Type != TokenType.ReservedSymbol)
                        throw new ParseException(string.Format("Expected +, *, - or /, but got {0}", nextToken), nextToken.Column);

                    if (operatorPriority == null || operatorPriority.CompareTo(nextToken.Symbol) < 0)
                    {
                        tokenReader.MoveNext();
                        AstNode secondExpression = ParseExpression(tokenReader, new OperatorComparable(nextToken.Symbol));
                        expression = new BinaryExpression(nextToken.Symbol, expression, secondExpression, nextToken.Column);
                    }
                    else
                        return expression;
                }
            }
        }

        public AstNode Parse(string text)
        {
            var reader = new TokenReader(text);

            AstNode node = ParseExpression(reader, null);

            if (reader.MoveNext())
                throw new ParseException(string.Format("Unexpected token '{0}', expected end of line", reader.Current), reader.Current.Column);

            return node;
        }
    }
}