﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hediet.SimpleMathExpressionInterpreter.Parser
{
    class ParseException : TextException
    {
        public ParseException(string message, int column)
            : base(message, column)
        {

        }
    }
}
