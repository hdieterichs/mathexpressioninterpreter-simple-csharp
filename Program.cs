﻿using System;
using Hediet.SimpleMathExpressionInterpreter.Logger;
using Hediet.SimpleMathExpressionInterpreter.Parser;
using Hediet.SimpleMathExpressionInterpreter.Interpreter;

namespace Hediet.SimpleMathExpressionInterpreter
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("----- Simple Math Expression Interpreter -----");
            Console.WriteLine("--------- By Henning Dieterichs 2012 ---------");
            Console.WriteLine("Please enter an expression like -(-5 + e) * PI\n");

            while (true)
            {
                Console.Write("> ");
                string expression = Console.ReadLine();

                try
                {
                    var parser = new ExpressionParser();
                    var tree = parser.Parse(expression);
                    Console.WriteLine("Recognized as: {0}\n", tree);

                    var interpreter = new ExpressionInterpreter(new ConsoleLogger());
                    var result = tree.Visit(interpreter);
                    Console.WriteLine("\n -> {0}", result);
                }
                catch (TextException e)
                {
                    Console.WriteLine(e.ToString());
                }

                Console.WriteLine("\n\n");
            }
        }
    }
}
