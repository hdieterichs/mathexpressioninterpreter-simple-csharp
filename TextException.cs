﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Hediet.SimpleMathExpressionInterpreter
{
    [Serializable]
    public class TextException : Exception
    {
        public TextException(string message, int column) : base(message)
        {
            Column = column;
        }

        public override string ToString()
        {
            return string.Format("{0} in column {1}", Message, Column);
        }

        public int Column { get; private set; }
    }
}
