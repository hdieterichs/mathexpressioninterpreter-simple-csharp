using System;

namespace Hediet.SimpleMathExpressionInterpreter.Tokenizer
{
    public class Token
    {
        public Token(string symbol, TokenType type, int column)
        {
            if (symbol == null) 
                throw new ArgumentNullException("symbol");
            Symbol = symbol;
            Type = type;
            Column = column;
        }

        public string Symbol { get; private set; }
        public TokenType Type { get; private set; }

        public int Column { get; private set; }

        public override string ToString()
        {
            return Symbol;
        }
    }
}