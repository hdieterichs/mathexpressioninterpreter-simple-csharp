﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hediet.SimpleMathExpressionInterpreter.Tokenizer
{
    class TokenException : TextException
    {
        public TokenException(string message, int column)
            : base(message, column)
        {

        }
    }
}
