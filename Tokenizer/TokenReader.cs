using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Hediet.SimpleMathExpressionInterpreter.Tokenizer
{
    public class TokenReader : IEnumerator<Token>, IEnumerable<Token>
    {
        public class Bookmark
        {
            private readonly TokenReader parent;
            private readonly int index;

            internal Bookmark(TokenReader parent)
            {
                if (parent == null) 
                    throw new ArgumentNullException("parent");
                this.parent = parent;
                this.index = parent.index;
            }

            public void Goto()
            {
                parent.index = index;
            }
        }

        public int Length 
        {
            get { return code.Length; }
        }

        private readonly string code;
        private int index;

        public TokenReader(string code)
        {
            if (code == null) 
                throw new ArgumentNullException("code");
            this.code = code;

            Reset();
        }

        public Bookmark GetBookmark()
        {
            return new Bookmark(this);
        }

        private char GetNextCharOrZero()
        {
            char c;
            do
            {
                index++;
                if (index >= code.Length)
                    return char.MinValue;
                c = code[index];
            } while (char.IsWhiteSpace(c));

            return c;
        }

        private Token NextToken()
        {
            char c = GetNextCharOrZero();

            int column = index + 1;

            if (c == char.MinValue)
                return null;

            string symbol = c.ToString(CultureInfo.InvariantCulture);
            
            if ("+*-/".Contains(c))
                return new Token(symbol, TokenType.ReservedSymbol, column);
            else if (c == '(')
                return new Token(symbol, TokenType.LeftParenthesis, column);
            else if (c == ')')
                return new Token(symbol, TokenType.RightParenthesis, column);
            else if (char.IsNumber(c))
            {
                bool decimalPoint = false;

                while (true)
                {
                    var last = GetBookmark();
                    c = GetNextCharOrZero();

                    if (char.IsDigit(c) || c == '.')
                        symbol += c;
                    else
                    {
                        last.Goto();
                        break;
                    }

                    if (c == '.')
                    {
                        if (decimalPoint)
                            throw new TokenException("Multiple decimal points found", column);

                        decimalPoint = true;
                    }
                }

                return new Token(symbol, TokenType.Number, column);
            }
            else if (char.IsLetter(c))
            {
                while (true)
                {
                    var last = GetBookmark();
                    c = GetNextCharOrZero();

                    if (char.IsLetter(c))
                        symbol += c;
                    else
                    {
                        last.Goto();
                        break;
                    }
                }

                return new Token(symbol, TokenType.Identifier, column);
            }
            else
                throw new TokenException(string.Format("Unexpected character found: {0}", c), index);
        }


        public void Dispose()
        {
            
        }

        public bool MoveNext()
        {
            Current = NextToken();
            return Current != null;
        }

        public Token Peek()
        {
            int i = index;
            Token result = NextToken();
            index = i;
            return result;
        }

        public void Reset()
        {
            index = -1;
        }

        public Token Current { get; private set; }

        object IEnumerator.Current
        {
            get { return Current; }
        }

        public IEnumerator<Token> GetEnumerator()
        {
            return this;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}