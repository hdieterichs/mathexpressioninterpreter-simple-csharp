using System;

namespace Hediet.SimpleMathExpressionInterpreter.Tokenizer
{
    [Flags]
    public enum TokenType
    {
        ReservedSymbol = 1,
        Identifier = 2,
        Number = 4,
        LeftParenthesis = 8,
        RightParenthesis = 16
    }
}